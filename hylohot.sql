
create table link (
    id integer primary key,
    url varchar(4096) not null,
    nick varchar(4096) not null,
    time timestamp not null,
    hash varchar(4096)
);

create unique index link_url_i on link (url);
create index link_hash_i on link (hash);

