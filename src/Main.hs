{-# LANGUAGE OverloadedStrings #-}

module Main (
    main
) where


import Formatting ((%), fprint, shown, text)
import System.Environment (getArgs)
import qualified Data.Text.Lazy as TL

import Hylohot (hylohot)


main :: IO ()
main = do
    args <- getArgs
    case length args of
        4 -> do
            fprint "Starting…\n"
            let host = TL.pack (args !! 0)
            let port = read (args !! 1)
            let nick = TL.pack (args !! 2)
            let chan = TL.pack (args !! 3)
            fprint ("Host: " % text % ".\n") host
            fprint ("Port: " % shown % ".\n") port
            fprint ("Nick: " % text % ".\n") nick
            fprint ("Chan: " % text % ".\n") chan
            hylohot host port nick chan
        _ -> do
            fprint "Usage: hylohot <host> <port> <nick> <chan>.\n"

