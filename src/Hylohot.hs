{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Hylohot (
    hylohot
) where


import Control.Monad (forM_, when)
import Data.Time.Clock (getCurrentTime)
import Data.Time.LocalTime (getCurrentTimeZone, utcToLocalTime)
import Formatting ((%), format, text)
import Formatting.Time (dateDash, hms)

import qualified Data.Text.Lazy as TL
import qualified Network.URI as NU

import Hylohot.Bot (Bot)
import qualified Hylohot.Bot as B
import qualified Hylohot.Common as C
import qualified Hylohot.DB as DB
import qualified Hylohot.HTTP as H
import qualified Hylohot.Proto as P


-- | Parse and check url, return Just url if url is correct and good.
getGoodURL :: TL.Text -> Maybe NU.URI
getGoodURL s =
    case NU.parseAbsoluteURI (TL.unpack s) of
        Just url ->
            if scheme == "http:" || scheme == "https:"
                then Just url
                else Nothing
            where
                scheme = NU.uriScheme url
        _ -> Nothing


-- | Check URL, first check by name and if not found, then check by contents.
checkURL :: TL.Text -> NU.URI -> Bot (Maybe DB.Link)
checkURL nick url = do
    let urlText = TL.pack $ NU.uriToString id url ""
    now <- B.io $ getCurrentTime
    mold1 <- B.runDB $ DB.checkURL urlText
    case mold1 of
        Just _ -> return mold1
        Nothing -> do
            muha <- B.io $ H.getURLHash url
            B.runDB $ DB.insertURLHash nick urlText now muha


sendOldURLInfo :: TL.Text -> DB.Link -> Bot ()
sendOldURLInfo _nick _old = do
    let _oldNick = P.nick $ DB.linkNick _old
    let _oldTime = DB.linkTime _old
    let _oldUrl = DB.linkUrl _old
    _tz <- B.io $ getCurrentTimeZone
    let _oldLocalTime = utcToLocalTime _tz _oldTime
    if _nick /= _oldNick
        then do
            let _fmt = text % ": STARE! " % text % " juz to wklejal: " % dateDash % " " % hms
            B.privmsg (format _fmt _nick _oldNick _oldLocalTime _oldLocalTime)
        else do
            let _fmt = text % ": STARE! sam juz to wklejales: " % dateDash % " " % hms
            B.privmsg (format _fmt _nick _oldLocalTime _oldLocalTime)


-- | URL in chat.
handleGoodURL :: TL.Text -> NU.URI -> Bot ()
handleGoodURL _from _url = do
    -- 1. YouTube
    let isYouTubeURL = C.getURLHost _url `elem` (map Just ["youtube.com", "www.youtube.com", "youtu.be"])
    when (isYouTubeURL) $ do
        mYouTubeTitle <- B.io $ H.getYouTubeTitle _url
        case mYouTubeTitle of
            Just t -> B.privmsg $ format ("        `-- " % text) t
            _ -> return ()
    -- 2. Old links
    mold <- checkURL _from _url
    case mold of
        Just old -> sendOldURLInfo (P.nick _from) old
        _ -> return ()


handle :: TL.Text -> Bot ()
handle x = do
    when (P.cmd x == "PRIVMSG") $ do
        let _msrc = P.prefix x
        case _msrc of
            Just _src -> do
                let _words = TL.words $ P.content x
                forM_ _words $ \word -> do
                    let murl = getGoodURL word
                    case murl of
                        Just url -> do
                            handleGoodURL _src url
                        _ -> return ()
            _ -> return ()


hylohot :: TL.Text -> Int -> TL.Text -> TL.Text -> IO ()
hylohot host port nick chan = B.bot host port nick chan handlers
    where
        handlers = [handle]



