{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Hylohot.Bot (
    Bot,
    bot,
    io,
    privmsg,
    runDB
) where


import Control.Concurrent (threadDelay)
import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader (ReaderT, ask, asks, runReaderT)
import Control.Exception (IOException, SomeException, bracket, catch, try)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Formatting ((%), format, fprint, int, shown, text)

import qualified Codec.Text.IConv as TI
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Encoding.Error as TEE
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TLE
import qualified Database.Persist.Class as DBPC
import qualified Database.Persist.Sql as DBPS
import qualified Database.Persist.Sqlite as DBPSL
import qualified Network as N
import qualified System.IO as SIO


data BotState = BotState { botSocket :: IORef (Maybe SIO.Handle)  -- Just if connected, Nothing if trying to connect
                         , botReconnectDelayMin :: Int
                         , botReconnectDelayMax :: Int
                         , botReconnectDelayCurrent :: IORef Int
                         , botHost :: TL.Text
                         , botPort :: Int
                         , botNick :: TL.Text
                         , botChan :: TL.Text
                         , botHandlers :: [TL.Text -> Bot ()]
                         , botDB :: DBPS.ConnectionPool }
type Bot = ReaderT BotState IO


bot :: TL.Text -> Int -> TL.Text -> TL.Text -> [TL.Text -> Bot ()] -> IO ()
bot host port nick chan handlers = bracket (initBot host port nick chan handlers) disconnect loop
    where
        connect = do
            result <- io $ try $ N.connectTo (TL.unpack host) (N.PortNumber (fromIntegral port))
            case result of
                Left (exception :: IOException) -> do
                    -- Delay and retry.
                    currentDelay <- asks botReconnectDelayCurrent >>= io . readIORef
                    io $ fprint
                        ("_ Connect failed, (exception: " % shown % "), retrying in " % int % " sec…\n")
                        exception
                        currentDelay
                    io $ threadDelay (currentDelay * 1000 * 1000)
                    growReconnnectDelay
                    connect
                Right h -> do
                    -- Connect successful: shrink retry delay (I don't want to set it to min right away),
                    -- set no-buffering, store sock in botstate.
                    shrinkReconnnectDelay
                    ref <- asks botSocket
                    io $ SIO.hSetBuffering h SIO.NoBuffering
                    io $ writeIORef ref (Just h)
        disconnect bs = do
            ms <- readIORef (botSocket bs)
            case ms of
                Just s -> SIO.hClose s
                _ -> return ()
        loop st = do
            fprint "_ Connecting…\n"
            runReaderT run st
            loop st
        run :: Bot ()
        run = do
            connect
            _nick <- asks botNick
            _chan <- asks botChan
            write "NICK" _nick
            write "USER" $ format (text % " 0 * :tut0ria1 b0t") _nick
            write "JOIN" _chan
            listen
        growReconnnectDelay = do
            -- TODO: modifyIORef'
            _ref <- asks botReconnectDelayCurrent
            _max <- asks botReconnectDelayMax
            _curr <- io $ readIORef _ref
            let _new = if _curr * 2 < _max then _curr * 2 else _max
            io $ writeIORef _ref _new
        shrinkReconnnectDelay = do
            -- TODO: modifyIORef'
            _ref <- asks botReconnectDelayCurrent
            _min <- asks botReconnectDelayMin
            _curr <- io $ readIORef _ref
            let _new = if _curr `quot` 8 > _min then _curr `quot` 8 else _min
            io $ writeIORef _ref _new


-- Send a privmsg to the current chan + server
privmsg :: TL.Text -> Bot ()
privmsg s = do
    chan <- asks botChan
    write "PRIVMSG" (format (text % " :" % text) chan s)


write :: TL.Text -> TL.Text -> Bot ()
write act params = do
    hr <- asks botSocket
    io $ do
        mh <- readIORef hr
        case mh of
            Just h -> do
                fprint ("> " % text % " " % text % "\n") act params
                let _t = format (text % " " % text % "\r\n") act params
                let _b = BL.toStrict $ TLE.encodeUtf8 _t
                B.hPut h _b
            Nothing -> do
                fprint "_ Can't send, no socket.\n"


connectDB :: IO DBPS.ConnectionPool
connectDB = do
        pool <- DBPC.createPoolConfig conf
        return pool
    where
        conf = DBPSL.SqliteConf { DBPSL.sqlDatabase = "hylohot.sqlite",
                                  DBPSL.sqlPoolSize = 8 }


runDB :: DBPS.SqlPersistT Bot x -> Bot x
runDB act = do
    db <- asks botDB
    r <- DBPS.runSqlPool act db
    return r


-- Connect to DB (we only connect once) and create initial bot state.
initBot :: TL.Text -> Int -> TL.Text -> TL.Text -> [TL.Text -> Bot ()] -> IO BotState
initBot host port nick chan handlers = do
    hr <- newIORef Nothing
    db <- connectDB
    initialDelayRef <- newIORef 1
    return $ BotState { botHost = host,
                        botPort = port,
                        botNick = nick,
                        botChan = chan,
                        botReconnectDelayMin = 1,
                        botReconnectDelayMax = 3600,
                        botReconnectDelayCurrent = initialDelayRef,
                        botSocket = hr,
                        botDB = db,
                        botHandlers = handlers }


io :: IO x -> Bot x
io = liftIO


-- Try with error catching, for handlers.
tryHandler :: (TL.Text -> Bot ()) -> TL.Text -> Bot ()
tryHandler act msg = do
        botState <- ask
        let ioact = runReaderT (act msg) botState
        liftIO $ catch ioact handler
    where
        handler (exception :: SomeException) = fprint ("_ Exception: " % shown % "\n") exception >> (SIO.hFlush SIO.stdout)


handle :: TL.Text -> Bot ()
handle t = do
    handlers <- asks botHandlers
    forM_ handlers $ \handler -> do
        tryHandler handler t


rstripNewLine :: TL.Text -> TL.Text
rstripNewLine = TL.reverse . TL.dropWhile _isnl . TL.reverse
    where
        _isnl c = (c == '\n' || c == '\r')


getTextLine :: SIO.Handle -> IO TL.Text
getTextLine h = do
    b <- B.hGetLine h
    case TLE.decodeUtf8' (BL.fromStrict b) of
        Left _ -> do
            let butf8 = TI.convertFuzzy TI.Discard "ISO-8859-2" "UTF-8" (BL.fromStrict b)
            let tutf8 = TLE.decodeUtf8With (TEE.replace '?') butf8
            return (rstripNewLine tutf8)
        Right _t -> do
            return (rstripNewLine _t)


-- | Process each line from the server
listen :: Bot ()
listen = do
        ref <- asks botSocket
        mh <- io $ readIORef ref
        case mh of
            Just h -> do
                rs <- io $ try $ getTextLine h -- TODO: detect encoding
                case rs of
                    Left (_exception :: IOException) -> io $ fprint ("_ Exception while reading from socket: " % shown % ".\n") _exception
                    Right _line -> do
                        io $ fprint ("< " % text % "\n") _line >> SIO.hFlush SIO.stdout
                        if ping _line then pong _line else handle _line
                        listen
            Nothing -> return () -- TODO: do not allow this at all
    where
        ping x    = "PING :" `TL.isPrefixOf` x
        pong x    = write "PONG" (format (":" % text) (TL.drop 6 x))


