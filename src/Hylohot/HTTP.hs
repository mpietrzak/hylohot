{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}


module Hylohot.HTTP (
    getURLHash,
    getYouTubeTitle
) where


import Control.Exception (try)
import Data.Int (Int64)
import Data.Text.Encoding.Error (lenientDecode)
import Data.Text.Lazy.Encoding (decodeUtf8With)
import Formatting ((%), format, fprint, hex, int, shown)
import Text.Regex.TDFA.ByteString.Lazy ()
import Text.Regex.TDFA ((=~))
import qualified Crypto.Hash.SHA1 as H
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Lazy as TL
import qualified Network.HTTP.Client as HC
import qualified Network.HTTP.Client.TLS as HCTLS
import qualified Network.URI as NU
import qualified Text.HTML.TagSoup as TS


hashDataSize :: Int64
hashDataSize = 1024 * 1024 -- 1 MiB


readSome :: Int64 -> IO B.ByteString -> IO BL.ByteString
readSome count reader = go ""
    where
        go sofar = do
            newchunk <- reader
            case newchunk of
                "" -> return sofar
                _ -> do
                    let newbuf = BL.append sofar (BL.fromStrict newchunk)
                    let newlen = BL.length newbuf
                    if newlen < count
                        then go newbuf
                        else return (BL.take count newbuf)


bsToHex :: B.ByteString -> TL.Text
bsToHex = TL.concat . map ctoh . B.unpack
    where
        ctoh = format hex


-- | Get hash as hex-string of initial part of contents referenced by given URL.
getURLHash :: NU.URI -> IO (Maybe TL.Text)
getURLHash uri = do
    let uriString = NU.uriToString id uri ""
    req <- HC.parseUrl uriString
    er <- try $ HC.withManager HCTLS.tlsManagerSettings $ \m -> do
        HC.withResponse req m $ \resp -> do
            let reader = HC.responseBody resp
            fprint "_ Reading...\n"
            content <- readSome hashDataSize reader
            fprint ("_ Got " % int % " bytes.\n") (BL.length content)
            let hash = H.hashlazy content
            let hashtext = bsToHex hash
            return hashtext
    case er of
        Left (_exception :: HC.HttpException) -> do
            fprint ("_ Got exception while trying to fetch " % shown % ": " % shown % ".\n") uri _exception
            return Nothing
        Right _h -> return (Just _h)


decodeHTMLEntities :: TL.Text -> TL.Text
decodeHTMLEntities t = case TS.parseTags t of
    [] -> ""
    (x:_) -> TS.fromTagText x


-- | Get YouTube video title.
-- This one is very similar to getURLHash. We should do some code reuse here.
getYouTubeTitle :: NU.URI -> IO (Maybe TL.Text)
getYouTubeTitle url = do
    let urlString = NU.uriToString id url ""
    req <- HC.parseUrl urlString
    HC.withManager HCTLS.tlsManagerSettings $ \m -> do
        HC.withResponse req m $ \resp -> do
            let reader = HC.responseBody resp
            content <- readSome (1024 * 1024) reader
            -- fastest and easiest way to extract contents of <title>…?
            let res = content =~ ("<title>([^<]+) - YouTube</title>" :: BL.ByteString) :: [[BL.ByteString]]
            case res of
                [[_,_m]] -> return $ Just $ decodeHTMLEntities $ decodeUtf8With lenientDecode _m
                _ -> return Nothing



