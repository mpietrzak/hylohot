{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}


module Hylohot.DB (
    getCount,
    checkURL,
    insertURLHash,
    Link,
    linkNick,
    linkTime,
    linkUrl
) where


import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Maybe (isJust)
import Database.Persist.Sql (SqlPersistT, Single(..), rawSql)
import Database.Persist (
    (=.),
    (==.),
    (!=.),
    Entity(..),
    SelectOpt(Asc, LimitTo),
    getBy,
    insertBy,
    selectList,
    update)
import Database.Persist.Sql (fromSqlKey)
import Data.Text.Lazy (Text)
import Data.Time.Clock (UTCTime)
import Formatting ((%), fprint, int, shown, text)
import qualified Database.Persist.TH as DBTH
import qualified Data.Text.Lazy as TL


DBTH.share [DBTH.mkPersist DBTH.sqlSettings] [DBTH.persistLowerCase|
Link
    url Text
    nick Text
    time UTCTime
    hash Text Maybe
    UniqueURL url
    deriving Show
|]


getCount :: MonadIO m => SqlPersistT m Int
getCount = do
        rows <- rawSql _query _params
        let cnt = case rows of
                [] -> 0
                (Single x:_) -> x
        return cnt
    where
        _query = "select count(1) from url"
        _params = []


-- Check if URL in DB, but do not check hash.
checkURL :: MonadIO m => TL.Text -> SqlPersistT m (Maybe Link)
checkURL url = do
    me <- getBy $ UniqueURL url
    return $ case me of
        Just (Entity _ v) -> Just v
        _ -> Nothing


insertURLHash :: MonadIO m => TL.Text -> TL.Text -> UTCTime -> Maybe TL.Text -> SqlPersistT m (Maybe Link)
insertURLHash nick url time mhash = do

    eold <- insertBy new
    case eold of
        Left (Entity dupKey dup) -> do
            -- link already exists; if it's hash is different and we have hash, then update
            liftIO $ fprint ("_ This link already exists, duplicate's key: " % shown % ".\n") dupKey
            when (isJust mhash && linkHash dup /= mhash) $ do
                update dupKey [LinkHash =. mhash]
            -- return what was already in DB
            return (Just dup)
        Right newKey -> do
            liftIO $ fprint ("_ New link saved at " % int % ".\n") (fromSqlKey newKey)
            case mhash of
                Just hash -> do
                    liftIO $ fprint ("_ Checking for others with the same hash (" % text % ").\n") hash
                    prevLinks <- selectList [LinkHash ==. mhash, LinkId !=. newKey] [LimitTo 1, Asc LinkTime]
                    case prevLinks of
                        [Entity _k prevLink] -> do
                            -- yep, there's dup
                            liftIO $ fprint
                                ("_ Found duplicate with key " % shown % " and time " % shown % ".\n")
                                _k
                                (linkTime prevLink)
                            return (Just prevLink)
                        _ -> do
                            liftIO $ fprint ("_ No duplicate.\n")
                            return Nothing -- no dup even by hash
                _ -> return Nothing -- insert successful, no new hash to check for, assume it's something new, return no dup
    where
        new = Link { linkUrl = url,
                     linkNick = nick,
                     linkTime = time,
                     linkHash = mhash }








