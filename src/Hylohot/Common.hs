{-# LANGUAGE OverloadedStrings #-}

module Hylohot.Common (
    getURLHost,
    traceValue
) where


import Formatting ((%), format, shown, text)
import qualified Data.Text.Lazy as TL
import qualified Debug.Trace as DT
import qualified Network.URI as NU


traceValue :: Show x => TL.Text -> x -> x
traceValue msg x = DT.trace s x
    where
        s = TL.unpack t
        t = format ("_ " % text % ": " % shown) msg x


getURLHost :: NU.URI -> Maybe TL.Text
getURLHost url = case NU.uriAuthority url of
    Just _au -> Just (TL.pack (NU.uriRegName _au))
    Nothing -> Nothing
