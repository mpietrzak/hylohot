{-# LANGUAGE OverloadedStrings #-}

module Hylohot.Proto where

import qualified Data.Text.Lazy as TL


prefix :: TL.Text -> Maybe TL.Text
prefix line =
    if ":" `TL.isPrefixOf` line then
        Just $ TL.takeWhile (/= ' ') $ TL.drop 1 line
    else
        Nothing


stripPrefix :: TL.Text -> TL.Text
stripPrefix line =
    if ":" `TL.isPrefixOf` line then
        TL.drop 1 $ TL.dropWhile (/= ' ') line
    else
        line


nick :: TL.Text -> TL.Text
nick = TL.takeWhile (/= '!')


cmd :: TL.Text -> TL.Text
cmd = TL.takeWhile (/= ' ') . stripPrefix


params :: TL.Text -> TL.Text
params = TL.drop 1 . TL.dropWhile (/= ' ') . TL.drop 1 . stripPrefix


-- | Technically not true, but who cares.
content :: TL.Text -> TL.Text
content = TL.drop 1 . TL.dropWhile (/= ':') . TL.drop 1

